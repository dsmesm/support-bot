let chatContainer = document.querySelector('.chat-container');

function showMessage(message) {
    let messageText = message.text;
    let time = new Date(message.date * 1000);
    let chatMessage = document.createElement('div');
    chatMessage.classList.add('message');
    let userName = message.from.is_bot ? 'You' : message.from.first_name;
    if (userName === 'You') {
        chatMessage.innerHTML = `<img src="img/user-icon.jpg" alt="User Icon" style="width: 100%; ">
                                 <span class="user-name">${userName}</span><p class="message-text">${messageText}</p>
                                 <span class="time-right">${(time.getHours() < 10 ? ('0' + time.getHours()) : time.getHours())}:${(time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes())}</span>`;
    } else {
        chatMessage.classList.add('darker');
        chatMessage.innerHTML = `<img src="img/support-icon.jpg" alt="User Icon" class="right" style="width: 100%">
                                 <span class="user-name">${userName}</span><pre class="message-text message-answer">${messageText}</pre>
                                 <span class="time-left">${(time.getHours() < 10 ? ('0' + time.getHours()) : time.getHours())}:${(time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes())}</span>`;
    }
    chatContainer.append(chatMessage);
    scroll();
    button.setAttribute('disabled','disabled');
}

function scroll() {
    chatContainer.scrollTop = 9999;
}

messageField.addEventListener('input', () => {
   if(messageField.value.trim().length === 0) {
       button.setAttribute('disabled','disabled');
   }
   else {
       button.removeAttribute('disabled');
   }
});