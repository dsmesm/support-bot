<?php ?>
<html>
<head>
    <title>Support bot</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="/img/favicon.png">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</head>
<body>
<div class="chat">
    <div class="container">
        <div class="chat-container"></div>
    </div>
    <div class="inputs-container">
        <form>
            <textarea name="user_message" id="user_message"  placeholder="type message..."></textarea>

            <input type="button" id="send_button" value="send" disabled>
        </form>
    </div>
</div>
<script src="index.js"></script>
<script src="front.js"></script>
<script>
    $('.inputs-container').keydown(function(e) {
        if (!e.shiftKey && e.keyCode === 13) {
            $('#send_button').click();
            e.preventDefault();
        }
    });
</script>
</body>
</html>